import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryS4Component } from './delivery-s4.component';

describe('DeliveryS4Component', () => {
  let component: DeliveryS4Component;
  let fixture: ComponentFixture<DeliveryS4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeliveryS4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryS4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
