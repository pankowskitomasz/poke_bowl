import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliveryRoutingModule } from './delivery-routing.module';
import { DeliveryComponent } from './delivery/delivery.component';
import { DeliveryS1Component } from './delivery-s1/delivery-s1.component';
import { DeliveryS2Component } from './delivery-s2/delivery-s2.component';
import { DeliveryS3Component } from './delivery-s3/delivery-s3.component';
import { DeliveryS4Component } from './delivery-s4/delivery-s4.component';


@NgModule({
  declarations: [
    DeliveryComponent,
    DeliveryS1Component,
    DeliveryS2Component,
    DeliveryS3Component,
    DeliveryS4Component
  ],
  imports: [
    CommonModule,
    DeliveryRoutingModule
  ]
})
export class DeliveryModule { }
