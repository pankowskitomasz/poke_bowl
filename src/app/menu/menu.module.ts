import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuRoutingModule } from './menu-routing.module';
import { MenuComponent } from './menu/menu.component';
import { MenubarComponent } from './menubar/menubar.component';
import { MenulistComponent } from './menulist/menulist.component';


@NgModule({
  declarations: [
    MenuComponent,
    MenubarComponent,
    MenulistComponent
  ],
  imports: [
    CommonModule,
    MenuRoutingModule
  ]
})
export class MenuModule { }
