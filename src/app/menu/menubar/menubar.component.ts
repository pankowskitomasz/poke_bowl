import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menubar',
  templateUrl: './menubar.component.html',
  styleUrls: ['./menubar.component.sass']
})
export class MenubarComponent implements OnInit {

  showFilters:boolean=true;

  showHideFilters(){
    this.showFilters=!this.showFilters;
  }
  
  constructor() { }

  ngOnInit(): void {
  }

}
